/**
 * Returns a number between 1-151 that we can use when making API calls to get a random pokemon
 */
function randomNumber(num: number){
 return Math.floor(Math.random() * num)
}

module.exports = randomNumber