/**
 * Uncomment the lines below to see some basic TypeScript Examples and how it can and can NOT be used
 */


// // Define Strings
// type Style = 'bold' | 'italic'
// let font: Style
// font = 'not bold or italic'

// // Define Objects
// interface Person {
//     first: string, 
//     last: string
// }

// // Passes Type Checks,
// const person1: Person = {
//     first: 'mike', 
//     last: 'chadwick'
// }

// // Does not pass type check 
// const person2: Person = {
//     first: 'Yang',
//     last: 'Kim',
//     isCool: true
// }

// // Arrays

// // Array of numbers
// const arr:number[] = []

// arr.push(1)
// arr.push('1')
// arr.push(true)

