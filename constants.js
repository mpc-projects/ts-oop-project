const party = [
  {
    name: "bulbasaur",
    id: 1,
    sprite:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png",
    level: 9,
    types: ["grass", "poison"],
    moves: ["bind", "sleep-talk", "attract", "growl"]
  },
  {
    name: "charmander",
    id: 4,
    sprite:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png",
    level: 69,
    types: ["fire", null],
    moves: ["protect", "wing-attack", "mud-slap", "confide"]
  },
  {
    name: "squirtle",
    id: 7,
    sprite:
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/7.png",
    level: 59,
    types: ["water", null],
    moves: ["facade", "dynamic-punch", "rock-smash", "aqua-jet"]
  }
];

module.exports = { party };
