"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Notice how we are using "import" and not the standard "require()" syntax
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
app.use(express_1.default.json());
// Import our Controller
const pokemonContr = require('./Controllers/Pokemon/index.js');
const PORT = 4001;
// Get a specific pokemon based off ID or default to a random one
app.get('/pokemon/:id?', pokemonContr.getPokemon);
// Get the 3 best pokemon to form our party
app.get('/party', pokemonContr.getParty);
// Catch a pokemon and add to our party
app.post('/catch', pokemonContr.catchPokemon);
// Rename a pokemon that is in our party
app.put('/rename/:id', pokemonContr.renamePokemon);
// Release a pokemon 
app.delete('/release/:id', pokemonContr.releasePokemon);
app.listen(PORT, () => {
    console.log(`WE RUNNING ON PORT ${PORT}`);
});
