import { Request, Response } from "express"
import axios from 'axios'
const randomNumber = require('../../functions/getRandomNumber.js')
const {party} = require('./../../constants.js')


const BASE_URL =  'https://pokeapi.co/api/v2/'

// Define our object types
interface Pokemon {
    name: string,
    id: number,
    sprite: string, 
    moves: Array<string>,
    level: number,
    types: Array<string>
}

// The current pokemon we are battling in the grass
let activePokemon: Pokemon

const pokemonController = {
    // Get some pokemon's
    async getPokemon(req: Request, res: Response){
     try {
        let pokemon
        const {id} = req.params

        // Get a specific pokemon if one is desired from the body
        if(id){
            pokemon = await axios.get(`${BASE_URL}/pokemon/${id}`).then((res: any) => {
                return res.data
            })
        } 
        // Otherwise get a random one
        else {
            const pokemonID: number =  randomNumber(151)
          
            pokemon = await axios.get(`${BASE_URL}/pokemon/${pokemonID}`).then((res: any) =>  res.data)
        }

       const moveLength: number = pokemon.moves.length 
       // Array containing the name of our moves
       const moves: Array<string> = []
       // Get 4 random moves from our API call.
       for(let i = 0; i < 4; i ++){
        moves.push(pokemon.moves[randomNumber(moveLength)].move.name )
       }
       // Only get data we care about
       const pokemonObj: Pokemon = {
           name: pokemon.name,
           id: pokemon.id,
           sprite: pokemon.sprites.front_default, 
           level: randomNumber(100), // Random level between 1-100
           types: [pokemon.types[0].type.name, pokemon.types[1]?.type.name || null],
           moves
       }

        activePokemon = {...pokemonObj}

        res.status(200).send(pokemonObj)
     } catch (error) {
        res.status(409).send(`Error! ${error}`)
     }
    },
    async getParty (req:  Request, res: Response)  {
        try {
            res.status(200).send(party)
        } catch (err) {
            res.status(409).send(`Error fetching Party: ${err}`)
        }
    },
    async catchPokemon(req: Request, res: Response) {
        try {
            // Add the current pokemon in the Grass to our party
          if(activePokemon){
            party.push(activePokemon)
            res.status(200).send(party)
          } else {
            res.status(404).send('You must find a a pokemon before you can catch one!')
          }
        } catch(err) { 
            res.status(409).send(`Failed to catch Pokemon! ${err}`)
        }
    },
    async renamePokemon(req: Request, res: Response) {
        try {
            const {id} = req.params
            const {newName} = req.body

            // Find our one we want to rename
            const updatedPokemon = party.find((pokemon: any) => pokemon.id === +id )    
            // Rename that bad boy
            updatedPokemon.name = newName
            // SEND IT
            res.status(200).send(party)
        } catch (err) {
            res.status(409).send(`Error renaming Pokemon: ${err}`)
        }
    }, 
    async releasePokemon(req: Request, res: Response){
        try {
            const {id} = req.params

            const updatedParty = party.filter((pokemon: any) => pokemon.id !== +id)

            res.status(200).send(updatedParty)
        } catch (error) {
            res.status(409).send(`Error! ${error}`)
        }
    }
}

module.exports = pokemonController