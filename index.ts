// Notice how we are using "import" and not the standard "require()" syntax
import express, { Application } from 'express';
const app:Application = express()
app.use(express.json())
// Import our Controller
const pokemonContr = require('./Controllers/Pokemon/index.js')

const PORT: number =  4001

// Get a specific pokemon based off ID or default to a random one
app.get('/pokemon/:id?', pokemonContr.getPokemon)
// Get the 3 best pokemon to form our party
app.get('/party', pokemonContr.getParty)
// Catch a pokemon and add to our party
app.post('/catch', pokemonContr.catchPokemon)
// Rename a pokemon that is in our party
app.put('/rename/:id', pokemonContr.renamePokemon)

// Release a pokemon 
app.delete('/release/:id', pokemonContr.releasePokemon)

app.listen(PORT, () => {
    console.log(`WE RUNNING ON PORT ${PORT}`)
})

